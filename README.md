# Preact Reducer Local Storage

A simple way to save a reducer generated with useReducer to local storages.

## Motivation

I wanted to save the state of a reducer to local storage, so I have build this library.

## Usage examples

An example can be seen in [`./example`](./example).
