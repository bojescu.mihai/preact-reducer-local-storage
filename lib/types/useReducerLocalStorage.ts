export type AbstractAction = { type: string; payload?: unknown };

export type PopulatedState<State = Record<string, unknown>> = State;
export type PopulateAction<State = Record<string, unknown>> = AbstractAction & { type: 'POPULATE'; payload: PopulatedState<State> };
export type AfterPopulateAction = AbstractAction & { type: 'AFTER_POPULATE' };
export type Dispatch = (action: PopulateAction | AfterPopulateAction) => void;
