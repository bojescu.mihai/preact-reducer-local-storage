import { AfterPopulateAction, PopulateAction } from '../../lib'

export type State = {
  value: number
}

export type Actions =
  | PopulateAction
  | AfterPopulateAction
  | { type: 'INCREMENT' }
  | { type: 'DECREMENT' }
