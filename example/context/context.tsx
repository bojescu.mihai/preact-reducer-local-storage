import { createContext, FunctionalComponent } from 'preact'
import { useReducer } from 'preact/hooks'
import { Actions, initialState, reducer, State } from '../reducer'
import { Reducer } from './types'
import { useSaveReducer } from '../../lib'

export const Context = createContext<Reducer<State, Actions>>({
  state: initialState,
  dispatch: () => undefined
})

export const Store: FunctionalComponent = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState)

  useSaveReducer(state, dispatch, initialState)

  return (
    <Context.Provider value={{ state, dispatch }} children={children} />
  )
}
