export type Reducer<State, Actions> = {
  state: State,
  dispatch: (actions: Actions) => void
}
